//
//  iOSDB.h
//  iOSDB
//
//  Created by Süleyman Çalık on 12/29/10.
//  Copyright 2010 Süleyman Çalık All rights reserved.
//

/**
 *  ******* Documented in 1394/04/01 VER:1.0.0 *******
 * Methods Count: 9
 *  Explain: this Helper Created to Edit or read from table fo Database
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "sqlite3.h"


@interface iOSDB : NSObject
{
    sqlite3 * database;
    BOOL isDatabaseOpen;
}
@property BOOL isDatabaseOpen;

/*
 
 */

/**
 Supports simple select queries like:
 SELECT a , b FROM todos WHERE a = 1 AND b = 2
 
 If elements argument is nil or empty array:
 SELECT * FROM todos WHERE a = 1 AND b = 2
 */

/**
 *  create object of This Class
 *
 *  @param name      name of database file like @"testDataBase"
 *  @param extension extension of database like @"db"
 *  @param version   version of database like @""
 *
 *  @return object of this Helper
 */
+(iOSDB*)setupWithFileName:(NSString *)name
                  extension:(NSString *)extension
                    version:(NSString *)version;

-(NSMutableArray *) selectWithQuery:(NSString *)query;
/**
 *  select some row from table
 *
 *  @param table    name of table to select
 *  @param elements name of columns to select
 *  @param keys     [key,values] that represent condition to select
 *
 *  @return : arrays of dictionary from selected rows
 */
-(NSArray *)selectFromTable:(NSString *)table
                   elements:(NSArray *)elements
                       keys:(NSDictionary *)keys;


/**
 *  insert one row in table
 *
 *  @param tableName name of table to insert
 *  @param elements  dictionary that keys are columns name and Value is row value for that column
 *
 *  @return YES -> if insertion is successfully
 */
-(BOOL)insertToTable:(NSString *)tableName
            elements:(NSDictionary *)elements;


/**
 *  update some rows from table
 *
 *  @param tableName  name of table to update
 *  @param controlKey dictionary of condition to select rows
 *  @param elements   [key,value] that keys are column name and values are updated value to change
 *
 *  @return YES -> if update is successfully
 */
-(BOOL)updateTable:(NSString *)tableName
    withControlKey:(NSDictionary *)controlKey
       andElements:(NSDictionary *)elements;

/**
 *  delete some rowse from table
 *
 *  @param table     name of table to delete
 *  @param key       name of column
 *  @param value     value of column
 *  @param isNumeric YES -> if value is numeric , NO -> if value is string
 *
 *  @return YES -> if delete is successfully
 */
-(BOOL)deleteFromTable:(NSString *)table
        withControlKey:(NSString *)key
              andValue:(NSString *)value
          andIsNumeric:(BOOL)isNumeric;

/**
 *  remove all rows of on table
 *
 *  @param table :name of table to clean
 *
 *  @return YES -> if cleaning was successfully
 */
-(BOOL)clearTable:(NSString *)table;

/**
 *  remove all rows of some tables
 *
 *  @param tables :names of tables to clean
 *
 *  @return YES -> if cleaning was successfully
 */
-(BOOL)clearTables:(NSArray *)tables;

/**
 *  remove all rowse of all tables in data base
 */
-(void)clearAllTables;

@end
