//
//  AppDelegate.h
//  iOSDB-To-FMDB
//
//  Created by IPA-Co on 6/17/16.
//  Copyright © 2016 IPA-Co. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

